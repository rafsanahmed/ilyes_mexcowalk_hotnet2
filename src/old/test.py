import glob
import os

inpath = '../out/connected_components_isolarge/'


#models_list = ['mexcowalk_intact']
#beta_values = [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]
#models = models = [m + '_beta_' + str(b).replace('.','') for m in models_list for b in beta_values]
models = ['mexcowalk_intact','mexcowalk_hint']
cosmic_file = '../data/Census_allTue_May_23_12-08-15_2017.tsv'

R = range(100,200,100)

outfile = '../out/cosmic_similarity/' + 'cosmic_comparison_hint_vs_inact.txt'

with open(cosmic_file, 'r') as f:
    cosmic_genes = [line.split()[0] for line in f.readlines()[1:]]
print cosmic_genes

with open(inpath + models[1] + '/cc_n100_35_12_d0.0010001122395116308.txt', 'r') as f:
    
    genes = set()
    for line in f.readlines():
        line = line.split()
        genes.update(line)

print len(set.intersection(genes, cosmic_genes))