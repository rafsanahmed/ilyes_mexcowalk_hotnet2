from utils import *
import glob
import os
import math
import operator

from tqdm import trange, tqdm
from matplotlib import pyplot as plt
#from matplotlib import rcParams
from pylab import *

ts= [5, 6, 7, 8,9]
models = ["mutex_t0{}_nsep_cov".format(t) for t in ts]
colors = ['C0', 'C1', 'C4', 'C2', 'C3']

def diff_mutex_thresh_plot():
    ps = ['iwavg_cov','wavg_mutex','wavg_covmutex']
    labels = ['Coverage Score (CS)', 'Mutual Exclusion Score ( MS )',  'Driver Module Set Score (DMSS)' ]
    for p,l in zip(ps,labels):
        Ns= []
        with open('../hint/out/evaluation_tab/{}.txt'.format(p)) as f:
            lines = f.readlines()
            models_ = lines[0].rstrip().split('\t')[1:]
            model2w = {s:[] for s in models_}
            for line in lines[1:]:
                line = line.rstrip().split('\t')
                Ns.append(int(line[0]))
                for m,w in zip(models_, line[1:]):
                    if float(w) == 0: continue
                    try:
                        model2w[m].append(float(w))
                    except:
                        print(m,w)

        axes(frameon=0)
        for m, c in zip(models, colors):
            try:
                plot(Ns, model2w[m], '-o', color = c)
            except:
                print(m, len(Ns), len(model2w[m]))

        art = []
        legend_ = ["MEXCOwalk_{}0{}".format(r'$\theta$',t) for t in ts]
        legend = plt.legend(legend_, loc=8,fancybox=True, fontsize= 'small', framealpha=0,
                            edgecolor = 'b', ncol= 3, bbox_to_anchor=(0.5,-0.3))
        art.append(legend)
        frame = legend.get_frame()
        plt.xlabel('total_genes')
        plt.ylabel(l)

        xtick = list(range(100,2600,200))#+[554]+ list(range(600,800, 200))+[806]+list(range(900, 2600, 200))
        xticks(xtick, fontsize='x-small')
        # plt.ylim(0.79, 1)
        # axes(frameon=0)
        grid()
        plt.savefig('../results/roc_MEXCOwalk/plots/{}.pdf'.format(p), format= 'pdf',additional_artists=art,
                    bbox_inches="tight", dpi = 800)
        plt.close()


def Logpval_per_comp():
    #models  = ["hotnet2","memcover_v1","memcover_v2","memcover_v3",
    #"mutex_t07_nsep_cov", 'hier_hotnet_k2', 'hier_hotnet_k3']
    # 'ClusterOne_0_0.9_0.6_k1', 'ClusterOne_0_0.9_0.6_k3']
    models = ['mexcowalk_orig', 'mexcowalk_beta_02']
    labels = models[:]
    #labels = [
        #"Hotnet2",
        #"MEMCover_v1",
        #"MEMCover_v2",
        #"MEMCover_v3",
        #"MEXCOwalk",
        #"HierHotnet_v1",
        #"HierHotnet_v2",
        ## 'ClusterOne_0_0.9_0.6_k1', 'ClusterOne_0_0.9_0.6_k3'
        #]
    model2label = {m:l for m,l in zip(models,labels )}

    model2w = {}
    with open('../out/cancer_subtype_test_results/Logpval_per_comp.txt') as f:
        lines = f.readlines()
        Ns = [int(n) for n in lines[0].rstrip().split('\t')]
        for line in lines[1:]:
            line = line.rstrip().split('\t')
            models_ = line[0]
            W = [float(w) for w in line[1:] if w != 'N/A']
            model2w[models_] = W
    model2w['hier_hotnet_k2'] = model2w['hier_hotnet'][1]
    model2w['hier_hotnet_k3'] = model2w['hier_hotnet'][0]
    axes(frameon=0)
    legend_ = []
    for m in models:
        legend_.append(model2label[m])
        if m == 'hier_hotnet_k2':
            plot([806], model2w[m], 'k*', markersize=12)
        elif m == 'hier_hotnet_k3':
            plot([554], model2w[m], 'C8*', markersize=12)

        elif m == 'memcover_v3':
            plot(Ns[:-9], model2w[m], '-o')
        else:
            try:
                plot(Ns, model2w[m], '-o')
            except:
                print(m, len(Ns), len(model2w[m]))

    art = []


    legend = plt.legend(legend_, loc=8,fancybox=True, fontsize= 'small', framealpha=0,
                        edgecolor = 'b', ncol= 4, bbox_to_anchor=(0.5,-0.25))
    art.append(legend)
    frame = legend.get_frame()
    plt.xlabel('total_genes')
    plt.ylabel('Cancer Type Specificity Score (CTSS)')

    xtick = list(range(100,2600,200))#+[554]+ list(range(600,800, 200))+[806]+list(range(900, 2600, 200))
    xticks(xtick, fontsize='x-small')
    plt.ylim(-0.01, 30)
    # axes(frameon=0)
    grid()
    plt.savefig('../hint/out/cancer_subtype_test_results_2/Logpval.pdf',format = 'pdf', additional_artists=art,
                bbox_inches="tight", dpi = 800)
    plt.close()


def max_size():


    models  = ["hotnet2",
    "memcover_v1","memcover_v2","memcover_v3",
    "mutex_t07_nsep_cov",
    'hier_hotnet'
    ]

    legend_ = [
        "Hotnet2",
        "MEMCover_v1",
        "MEMCover_v2",
        "MEMCover_v3",
        "MEXCOwalk",
        "HierHotnet_v1",
        "HierHotnet_v2",
        ]
    component_path = '../hint/out/connected_components_isolarge_n2500_whh/'
    N = list(range(100,600,100))+[554]+ list(range(600,800, 100))+[806]+list(range(900, 2600, 100))
    memcover_v3_range = list(range(100,600,100))+[554]+ list(range(600,800, 100))+[806]+list(range(900, 1700, 100))
    for model in models:
        delimiter = '\t' if 'hier' in model or  'memcover' in model else ' '
        res = []
        max_size = []
        for n in N:
            if (n not in [554, 806] and model == 'hier_hotnet') or (n not in memcover_v3_range and model == 'memcover_v3'):
                continue
            else:
                file = glob.glob(component_path +'{}/cc_n{}_*'.format(model, n))[0]
                # print('file')
                with open(file, 'r') as f:
                    lines = [len(l.strip().split(delimiter)) for l in f.readlines()]
                    max_ = max(lines)
                    max_modules = [1 for m in lines if m == max_]

                    res.append(max_*len(max_modules)/n)
                    max_size.append(max_)
        print(model,' ', max_size)
        axes(frameon=0)
        if model == 'hier_hotnet':
            plot([806], res[1], 'k*', markersize=12)
            plot([554], res[0], 'C8*', markersize=12)
        elif model == 'memcover_v3':
            plot(memcover_v3_range, res, '-o')
        else:
            plot(N, res,  '-o')

    art = []


    legend = plt.legend(legend_, loc=8,fancybox=True, fontsize= 'small', framealpha=0,
                        edgecolor = 'b', ncol= 4, bbox_to_anchor=(0.5,-0.3))
    art.append(legend)
    frame = legend.get_frame()
    plt.xlabel('total_genes')
    plt.ylabel('Percentage of Genes in Max Sized Modules')

    xtick = list(range(100,2600,200))#+[554]+ list(range(600,800, 200))+[806]+list(range(900, 2600, 200))
    xticks(xtick, fontsize='x-small')
    # plt.ylim(-0.01, 40)
    # axes(frameon=0)
    grid()
    plt.savefig('../results/main plots/max_size_over_N.pdf',format = 'pdf', additional_artists=art,
                bbox_inches="tight", dpi = 800)
    plt.close()



def min_size_modules():


    models  = ["hotnet2",
    "memcover_v1","memcover_v2","memcover_v3",
    "mutex_t07_nsep_cov",
    'hier_hotnet'
    ]

    legend_ = [
        "Hotnet2",
        "MEMCover_v1",
        "MEMCover_v2",
        "MEMCover_v3",
        "MEXCOwalk",
        "HierHotnet_v1",
        "HierHotnet_v2",
        ]
    # print(glob.glob('../results/results/*'))
    component_path = '../hint/out/connected_components_isolarge_n2500_whh/'
    N = list(range(100,600,100))+[554]+ list(range(600,800, 100))+[806]+list(range(900, 2600, 100))
    memcover_v3_range = list(range(100,600,100))+[554]+ list(range(600,800, 100))+[806]+list(range(900, 1700, 100))
    for model in models:
        delimiter = '\t' if 'hier' in model or  'memcover' in model else ' '
        res = []
        min_size = []
        for n in N:
            if (n not in [554, 806] and model == 'hier_hotnet') or (n not in memcover_v3_range and model == 'memcover_v3'):
                continue
            else:
                file = glob.glob(component_path +'{}/cc_n{}_*'.format(model, n))[0]
                # print('file')
                with open(file, 'r') as f:
                    lines = [len(l.strip().split(delimiter)) for l in f.readlines()]
                    min_ = min(lines)
                    min_modules = [1 for m in lines if m == min_]

                    res.append(min_*len(min_modules)/n)
                    min_size.append(min_)

        print(model,' ', min_size)
        # print(len(min_size), ' ', len(N))
        axes(frameon=0)
        if model == 'hier_hotnet':
            plot([806], res[1], 'k*', markersize=12)
            plot([554], res[0], 'C8*', markersize=12)
        elif model == 'memcover_v3':
            plot(memcover_v3_range, res, '-o')
        else:
            plot(N, res,  '-o')

    art = []


    legend = plt.legend(legend_, loc=8,fancybox=True, fontsize= 'small', framealpha=0,
                        edgecolor = 'b', ncol= 4, bbox_to_anchor=(0.5,-0.3))
    art.append(legend)
    frame = legend.get_frame()
    plt.xlabel('total_genes')
    plt.ylabel('Percentage of Genes in Min Sized Modules')

    xtick = list(range(100,2600,200))#+[554]+ list(range(600,800, 200))+[806]+list(range(900, 2600, 200))
    xticks(xtick, fontsize='x-small')
    # plt.ylim(-0.01, 40)
    # axes(frameon=0)
    grid()
    plt.savefig('../results/main plots/min_size_over_N.pdf',format = 'pdf', additional_artists=art,
                bbox_inches="tight", dpi = 800)
    plt.close()


# diff_mutex_thresh_plot()

Logpval_per_comp()
# print('Max size for each N')
#max_size()
# print('Min size for each N')
#min_size_modules()
