import glob
import os

inpath = '../data/'

infile_tcga = inpath + 'BRCA.rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes_normalized__data.data.txt'
infile_hint = inpath + 'hint_indices.txt'
infile_cosmic = inpath + 'Census_allTue_May_23_12-08-15_2017.tsv'
infile_mut = inpath + 'genes_vs_patients.txt'

with open(infile_cosmic, 'r') as f:
    
    cosmic_genes = [line.split()[0] for line in f.readlines()[1:]]
    
with open(infile_mut, 'r') as f:
    
    mut_data_genes = [line.split()[0] for line in f.readlines()]

with open(infile_hint, 'r') as f:
    
    hint_genes = []
    for line in f.readlines():
        line = line.split()
        hint_genes.append(line[1].upper())
print len(hint_genes)

with open(infile_tcga, 'r') as f:
    
    tcga_genes = []
    for line in f.readlines()[2:]:
        line = line.split()
        g = line[0].split('|')[0]
        if g != '?':
            
            tcga_genes.append(g.upper())
print len(tcga_genes)
#print tcga_genes
gg = []
for g in hint_genes:
    if g not in tcga_genes:
        gg.append(g)
#print len(gg)
#print gg

print 'tcga',len(set.intersection(set(cosmic_genes), set(tcga_genes)))
print 'hint',len(set.intersection(set(cosmic_genes), set(hint_genes)))
print 'mut',len(set.intersection(set(cosmic_genes), set(mut_data_genes)))
print 'all',len(set.intersection(set(cosmic_genes), set(hint_genes), set(tcga_genes)))

print sorted(set.intersection(set(cosmic_genes), set(hint_genes)))