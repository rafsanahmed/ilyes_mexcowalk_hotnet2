from collections import OrderedDict
import pandas as pd
import matplotlib.pyplot as plt
inpath  = '../out/evaluation_tab/'
infile = inpath+'num_components.txt'
plot_path = '../out/figures/'

d = OrderedDict()

#with open(infile, 'r') as f:
    
    #for line in f.readlines()[1:]:
        #line = line.split()
        #d[int(line[0])] = []
        #for v in line[1:]:
            #d[int(line[0])].append(int(v))
#print d
models = ['mexcowalk_orig', 'mexcowalk_beta_02']
colors = ['C0','C1']
legend_ = models[:]

#df = pd.read_table(infile, sep='\t',header=0, skipinitialspace=True, index_col=0, usecols=models, dtype=int)
df = pd.read_table(infile, sep='\t', index_col=0,header=0)

#plt.figure(figsize=(8,8))
plt.axes(frameon=0)
plt.grid()
plt.xticks(range(100,2600,200))
plt.ylim(6, 10.0)
for m in range(len(models)):
     index = df.index.tolist()
     vals = df[models[m]].tolist()
     avg_mod_size = [float(index[k])/float(vals[k]) for k in range(len(index))]
     print index
     print vals
     print avg_mod_size
     plt.plot(index, avg_mod_size, '-o'+colors[m])
     
art = []


legend = plt.legend(legend_, loc=8,fancybox=True, fontsize= 'small', framealpha=0,
                   edgecolor = 'b', ncol= 4, bbox_to_anchor=(0.5,-0.2))
art.append(legend)
frame = legend.get_frame()

#plt.show()

plt.savefig('../out/figures/avg_mod_size.pdf',format = 'pdf', additional_artists=art,
            bbox_inches="tight", dpi = 800)
plt.close()