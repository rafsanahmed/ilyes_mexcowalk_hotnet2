import glob
import os

inpath = '../out/connected_components_isolarge/'


models_list = ['mexcowalk_intact']
#beta_values = [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]
#models = models = [m + '_beta_' + str(b).replace('.','') for m in models_list for b in beta_values]
models = ['mexcowalk_intact','mexcowalk_hint']

R = range(100,2600,100)

orig_path = inpath + models_list[0] + '/'
outfile = '../out/' + 'PPI_comparison_percentage_intact_vs_hint_intersection.txt'

temp = sorted(glob.glob(orig_path+'cc*.txt'))
orig_files = [os.path.basename(f) for f in temp]

d_orig = {}

for ff in orig_files:
    n = ff.split('_')[1][1:]
    d_orig[int(n)] = []
    with open(orig_path + ff, 'r') as f:
        for line in f.readlines():
            line = line.split()
            d_orig[int(n)].append(set(line))

#for k in sorted(d_orig):
    #sums = 0
    #for s in d_orig[k]:
        #sums+=len(s)
        
    #print k, sums, len(d_orig[k])
    
#comparison
d={}
for model in models:
    d[model] = {}
    
    temp = sorted(glob.glob(inpath+ model+'/cc*.txt'))
    files_list = [os.path.basename(f) for f in temp]
    
    for ff in files_list:
        n = ff.split('_')[1][1:]
        d[model][int(n)] = []
        with open(inpath + model+ '/' + ff, 'r') as f:
            for line in f.readlines():
                line = line.split()
                d[model][int(n)].append(set(line))


with open(outfile, 'w') as f:          
    f.write('N')
    for model in models:
        f.write('\t' + model)
    f.write('\n')
    
    for i in R:
        f.write(str(i))
        
        for model in models:
            if i in d[model] and i in d_orig:
                #this part may be for checking components
                #for comp in d_orig[i]:
                    #if comp not in d[model][i]:
                        
                set_beta = set()
                set_orig = set()
                
                for s in d_orig[i]:
                    set_orig.update(s)
                for s in d[model][i]:
                    set_beta.update(s)
                
                f.write('\t' + str(len(set.intersection(set_orig, set_beta))))
                
                #diff = len(set_orig) - len(set.intersection(set_orig, set_beta))
                
                #f.write('\t' + str(float(diff)*100/float(len(set_orig))))
                
        f.write('\n')
    
