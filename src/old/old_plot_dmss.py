import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

outpath = '../out/figures/'
cosmic_file = '../out/evaluation_tab/iwcov_wmex.txt'

models = ['mexcowalk_orig', 'mexcowalk_beta_02']
colors = ['C0', 'C1']

df = pd.read_table(cosmic_file, sep='\t', header=0, index_col=0)
columns = list(df)

plt.figure(figsize=(10, 8))
plt.axes(frameon=0)
plt.xlabel('total_genes')
plt.ylabel('Diver Module Set Score (DMSS)')
plt.xticks(np.arange(100, 2600, step=200))
#plt.ylim(0.02, 0.200)
plt.yticks(np.arange(0, 0.2, step=0.025))
   
index_list = [float (m) for m in df.index.tolist()]
plt.xticks(index_list,  fontsize=7)
for c in range(len(models)):

    col = [float (m) for m in df[models[c]].tolist()]
    plt.plot(index_list,col, '-o'+colors[c])

plt.grid()

plt.legend(models[:],loc=8, fancybox=True, fontsize= 'small', framealpha=0,
                    edgecolor = 'b',ncol= 2, bbox_to_anchor=(0.5,-0.125))
plt.frameon = False
#plt.show()
plt.savefig(outpath+'DMSS.pdf',format = 'pdf',
                bbox_inches="tight", dpi = 800)