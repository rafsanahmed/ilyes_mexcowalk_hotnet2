import glob
import os

inpath = '../out/connected_components_isolarge/'


#models_list = ['mexcowalk_intact']
#beta_values = [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]
#models = models = [m + '_beta_' + str(b).replace('.','') for m in models_list for b in beta_values]
models = ['mexcowalk_intact','mexcowalk_hint']
cosmic_file = '../data/Census_allTue_May_23_12-08-15_2017.tsv'

R = range(100,2600,100)

outfile = '../out/cosmic_similarity/' + 'cosmic_comparison_hint_vs_inact.txt'

with open(cosmic_file, 'r') as f:
    cosmic_genes = [line.split()[0] for line in f.readlines()[1:]]

#comparison
d={}
for model in models:
    d[model] = {}
    
    temp = sorted(glob.glob(inpath+ model+'/cc*.txt'))
    files_list = [os.path.basename(f) for f in temp]
    
    for ff in files_list:
        n = int(ff.split('_')[1][1:])
        if n in R:
            d[model][int(n)] = []
            with open(inpath + model+ '/' + ff, 'r') as f:
                for line in f.readlines():
                    line = line.split()
                    d[model][int(n)].append(set(line))

#for m in d:
    #for k in sorted(d[m]):
        #print m, k, len(d[m][k])


d_cosm = {}
for model in models:
    print model
    d_cosm[model] = {}
    
    
    for n in sorted(d[model]):
        d_cosm[model][n] = []
        
        for comps in d[model][n]:
            for g in comps:
                if g.upper() in cosmic_genes:
                    d_cosm[model][n].append(g)

#for m in d_cosm:
    #for k in sorted(d_cosm[m]):
        #print m,k,len(d_cosm[m][k])

d_cosm['intersection'] = {}
print 'intersection'
for r in R:
    x = set(d_cosm[models[0]][r])
    y = set(d_cosm[models[1]][r])
    s = set.intersection(x, y)
    print len(x),len(y),len(s)
    d_cosm['intersection'][r] = s

for k in sorted(d_cosm['intersection']):
    print k, len(d_cosm['intersection'][k])


with open(outfile, 'w') as f:          
    f.write('N')
    for model in models:
        f.write('\t' + model)
    f.write('\tcosmic_intersection\n')
    
    for i in R:
        f.write(str(i))
        
        for model in models + ['intersection']:
            
                
            f.write('\t' + str(len(d_cosm[model][i])))
                                
        f.write('\n')
    
