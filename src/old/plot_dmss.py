from utils import *
import glob
import os
import math
import operator

from tqdm import trange, tqdm
from matplotlib import pyplot as plt
#from matplotlib import rcParams
from pylab import *

#ts= [5, 6, 7, 8,9]
#models = ["mutex_t0{}_nsep_cov".format(t) for t in ts]
models = ['mexcowalk_hint', 'mexcowalk_intact']
colors = ['C4', 'C1']#, 'C4', 'C2', 'C3']

def diff_mutex_thresh_plot():
    ps = ['iwcov_wmex'] #'iwavg_cov','wavg_mutex',
    labels = [ 'Driver Module Set Score (DMSS)' ] #'Coverage Score (CS)', 'Mutual Exclusion Score ( MS )',
    for p,l in zip(ps,labels):
        Ns= []
        with open('../out/evaluation_tab/{}.txt'.format(p)) as f:
            lines = f.readlines()
            models_ = lines[0].rstrip().split('\t')[1:]
            model2w = {s:[] for s in models_}
            for line in lines[1:]:
                line = line.rstrip().split('\t')
                Ns.append(int(line[0]))
                for m,w in zip(models_, line[1:]):
                    if float(w) == 0: continue
                    try:
                        model2w[m].append(float(w))
                    except:
                        print(m,w)

        axes(frameon=0)
        for m, c in zip(models, colors):
            try:
                plot(Ns, model2w[m], '-o', color = c)
            except:
                print(m, len(Ns), len(model2w[m]))

        art = []
        legend_ = [
        'MEXCOWalk_HINT',
        'MEXCOWalk_IntAct'
        ]#["MEXCOwalk_{}0{}".format(r'$\theta$',t) for t in ts]
        legend = plt.legend(legend_, loc=8,fancybox=True, fontsize= 'small', framealpha=0,
                            edgecolor = 'b', ncol= 3, bbox_to_anchor=(0.5,-0.3))
        art.append(legend)
        frame = legend.get_frame()
        plt.xlabel('total_genes')
        plt.ylabel(l)

        xtick = list(range(100,2600,200))#+[554]+ list(range(600,800, 200))+[806]+list(range(900, 2600, 200))
        xticks(xtick, fontsize='x-small')
        # plt.ylim(0.79, 1)
        # axes(frameon=0)
        grid()
        plt.savefig('../out/figures/{}.pdf'.format(p), format= 'pdf',additional_artists=art,
                    bbox_inches="tight", dpi = 800)
        plt.close()

diff_mutex_thresh_plot()