from utils import *
import glob
import os
import math
import operator

from tqdm import trange, tqdm
from matplotlib import pyplot as plt
#from matplotlib import rcParams
from pylab import *

#ts= [5, 6, 7, 8,9]
#models = ["mutex_t0{}_nsep_cov".format(t) for t in ts]
colors = ['C4', 'C1']#', 'C4', 'C2', 'C3']
def Logpval_per_comp():
    #models  = ["hotnet2","memcover_v1","memcover_v2","memcover_v3",
    #"mutex_t07_nsep_cov", 'hier_hotnet_k2', 'hier_hotnet_k3']
    # 'ClusterOne_0_0.9_0.6_k1', 'ClusterOne_0_0.9_0.6_k3']
    models = ['mexcowalk_hint', 'mexcowalk_intact']
    labels = [
        'MEXCOWalk_HINT',
        'MEXCOWalk_IntAct'
        ]
    #labels = [
        #"Hotnet2",
        #"MEMCover_v1",
        #"MEMCover_v2",
        #"MEMCover_v3",
        #"MEXCOwalk",
        #"HierHotnet_v1",
        #"HierHotnet_v2",
        ## 'ClusterOne_0_0.9_0.6_k1', 'ClusterOne_0_0.9_0.6_k3'
        #]
    model2label = {m:l for m,l in zip(models,labels )}

    model2w = {}
    with open('../out/cancer_subtype_test_results/Logpval_per_comp.txt') as f:
        lines = f.readlines()
        Ns = [int(n) for n in lines[0].rstrip().split('\t')]
        for line in lines[1:]:
            line = line.rstrip().split('\t')
            models_ = line[0]
            W = [float(w) for w in line[1:] if w != 'N/A']
            model2w[models_] = W
    #model2w['hier_hotnet_k2'] = model2w['hier_hotnet'][1]
    #model2w['hier_hotnet_k3'] = model2w['hier_hotnet'][0]
    axes(frameon=0)
    legend_ = []
    for m in models:
        legend_.append(model2label[m])
        if m == 'hier_hotnet_k2':
            plot([806], model2w[m], 'k*', markersize=12)
        elif m == 'hier_hotnet_k3':
            plot([554], model2w[m], 'C8*', markersize=12)

        elif m == 'memcover_v3':
            plot(Ns[:-9], model2w[m], '-o')
        else:
            try:
                plot(Ns, model2w[m], '-o'+colors[models.index(m)])
            except:
                print(m, len(Ns), len(model2w[m]))

    art = []


    legend = plt.legend(legend_, loc=8,fancybox=True, fontsize= 'small', framealpha=0,
                        edgecolor = 'b', ncol= 4, bbox_to_anchor=(0.5,-0.25))
    art.append(legend)
    frame = legend.get_frame()
    plt.xlabel('total_genes')
    plt.ylabel('Cancer Type Specificity Score (CTSS)')

    xtick = list(range(100,2600,200))#+[554]+ list(range(600,800, 200))+[806]+list(range(900, 2600, 200))
    xticks(xtick, fontsize='x-small')
    plt.ylim(5, 30)
    # axes(frameon=0)
    grid()
    plt.savefig('../out/figures/CTSS2.pdf',format = 'pdf', additional_artists=art,
                bbox_inches="tight", dpi = 800)
    plt.close()

Logpval_per_comp()