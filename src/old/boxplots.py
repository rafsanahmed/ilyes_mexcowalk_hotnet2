import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import operator
import os

def draw_plot(data, edge_color, fill_color, N, positions):
    bp = ax.boxplot(data, patch_artist=True, widths = 8, whis = 1, labels = [str(fs) for fs in N],
                    sym = ''.format(edge_color), manage_xticks = False, positions = positions,
                    meanprops =dict(color='black') ) # boxprops=dict(facecolor=edge_color),


    
    for element in ['boxes', 'whiskers', 'medians', 'caps']: #'boxes', 'whiskers', 'fliers', 'medians', 'caps'
        plt.setp(bp[element], color=edge_color)

    plt.setp(bp['fliers'], color=edge_color, markersize = 4)        
    plt.setp(bp['medians'], color='black')        
    return bp["boxes"][0]

if __name__ == '__main__':
    
    edge_colors = ['C0', 'C1',]#['C0', 'C1','C2', 'C3', 'C4', 'C6'] #['b', 'g', 'r', 'c', 'm', 'y']
    fill_colors = ['C0', 'C1',]#['C0', 'C2', 'C3', 'C4', 'C6', 'C7']
    
    # models = [
    #         "hotnet2",
    #         "memcover_v1","memcover_v2","memcover_v3",
    #         "mutex_t07_nsep_cov",
    #         'hier_hotnet'
    #          ]
    # legend_ = [
    #         "Hotnet2",
    #         "MEMCover_v1",
    #         "MEMCover_v2",
    #         "MEMCover_v3",
    #         "MEXCOwalk",
    #         "HierHotnet_v2"]
    
    models = [
        'mexcowalk_orig',
        'mexcowalk_beta_02'
    ]
    
    legend_ = [
        'mexcowalk_orig',
        'mexcowalk_beta_02'
    ]
    plt.figsize= (80,80)
    fig, ax = plt.subplots()
    
    legend_artists = []
    
    ranges = [range(100,900,100), range(900,1700,100), range(1700,2600,100)]
    for rr in range(len(ranges)):
        
        for idx,(model,edge_color, fill_color,l) in enumerate(zip(models, edge_colors, fill_colors,legend_)):
            print(l)
            sores_per_n = []
            N = ranges[rr]#ist(range(100,600,100))+ list(range(600,900, 100)) #+[554]
        #     N = a + list(range(900, 1700, 100)) if model  == 'memcover_v3' else a+list(range(900, 2600, 100))
            ticks = [(f+1)*100 for f in range(len(N))]
            if not model == 'hier_hotnet':
        
                for n in N:
                    results_file = '../results/knearst/revision/undersampel/{}/{}.txt'.format(model, n)
                    with open(results_file, 'r') as f:
                        lines= f.readlines()
                        sores_per_n.append([float(s) for s  in lines[-1].rstrip().split(' ')[1:]])
        
                positions = [i_+(idx-3)*10 for i_ in ticks]
                print positions
        
                l_art = draw_plot(sores_per_n, edge_color, None, N, positions)
                legend_artists.append(l_art)
        
            else:
                
                N_ = 554
        
                # for n in [554,806]:
                results_file = '../results/knearst/revision/undersampel/{}/{}.txt'.format('hier_hotnet', N_)
                with open(results_file, 'r') as f:
                    lines= f.readlines()
                    sores_per_n.append([float(s) for s  in lines[-1].rstrip().split(' ')[1:]])
        
                scores =  [[] for _ in range(100,600,100)]+sores_per_n
                print('554 plot')
                print(len(N), len(scores))
                positions = [i_+(idx-3)*10 for i_ in ticks[:6]]
                print('554 positions ', positions)
                l_art = draw_plot(scores, 'C8', None,  N[:6] ,positions)
                legend_artists.append(l_art)
                    
        
        
        

        art = []
        xtick = ticks
        xticks(xtick, fontsize='x-small')
        
        ax.set_xticklabels(N, fontdict = {'fontsize':'x-small'})
        ax.legend(legend_artists,legend_ , loc=8,fancybox=True, fontsize= 'small', framealpha=0,
                            edgecolor = 'b', ncol= 4, bbox_to_anchor=(0.5,-0.3))
        
        # legend = plt.legend(legend_, loc=8,fancybox=True, fontsize= 'small', framealpha=0,
        #                     edgecolor = 'b', ncol= 4, bbox_to_anchor=(0.5,-0.3))
        # art.append(legend)
        # frame = legend.get_frame()
        # frame.set_facecolor('0.9')
        # frame.set_edgecolor('0.9')
        
        #cc
        #plt.xlabel('total_genes')
        #plt.ylabel('Classification Accuracy')
        # grid()
        
        #plt.savefig('../results/knearst/revision/plots/Boxplots_Accuracy_0_{}.pdf'.format(rr), format = 'pdf',additional_artists=art,
                            #bbox_inches="tight", dpi = 800)
        
        #plt.show()    