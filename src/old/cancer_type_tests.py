import scipy.stats as stats
import math
import os
import glob
import numpy as np
from statsmodels.stats.multitest import fdrcorrection
# 'ClusterOne_0_0.9_0.6_k3',
#models = [
        #"hotnet2",
        #"memcover_v1","memcover_v2","memcover_v3",
        #"mutex_t07_nsep_cov",
        #'hier_hotnet'
        #]
models = ['mexcowalk_orig', 'mexcowalk_beta_02']
##ns = 1
##ne = 50
#models_pre = ['mexcowalk_cov', 'mexcowalk_t05', 'mexcowalk_t06', 'mexcowalk_t08', 'mexcowalk_t09', 'mexcowalk_t10']



num_subtypes = 11 #combine CRC
subtypes = ['BLCA', 'BRCA', 'CRC', 'GBM', 'HNSC', 'KIRC', 'LAML', 'LUAD', 'LUSC', 'OV', 'UCEC']
#for m in models_pre:
   #for c in subtypes:

      #models.append(c +'_' + m)
   
def count_subtype(patient_list):
   count_vec = [0] *  num_subtypes
   for patient in patient_list:
      subtype = patient_types[patient]
      index = subtypes.index(subtype)
      count_vec[index] += 1

   return count_vec



fhindices = open("../data/patients_to_indices_gen.txt")

ftype = open("../data/our_patients_subtypes.txt")


# read the patients / types file

count_types_each = [0] * num_subtypes
patient_types = {}
for line in ftype:
   words = line.strip().split()
   patient_types[words[0]] = words[1]
   subtype_index = subtypes.index(words[1])
   count_types_each[subtype_index] += 1

print (count_types_each)
count_subtypes_except_this = [0] * num_subtypes
for i in range(num_subtypes):
	count_subtypes_except_this[i] = sum(count_types_each) - count_types_each[i]

print ('sum(count_types_each): ',sum(count_types_each))
print ('count_subtypes_except_this: ',count_subtypes_except_this)

dict_indices = {}
for line in fhindices:
   words = line.strip().split()
   dict_indices[words[1]] = words[0]


   

#entry_11 entry_12
#entry_21 entry_22

list_range = list(range(100,2600,100))
#list_range = list(range(100, 600, 100))
#list_range.append(554)
#list_range.extend([600,700,800])
#list_range.append(806)
#list_range.extend(list(range(900,1601,100)))
#memcover_v3_range = list_range[:]
#list_range.extend(list(range(1700,2501,100)))
#print (list_range)


common_path = '../out/connected_components_isolarge/'
output_path = '../out/cancer_subtype_test_results/'


fhout_signif_per_comp = open(output_path + 'Signif_tests_per_comp.txt', 'w')
fhout_logpval_per_comp = open(output_path + 'Logpval_per_comp.txt', 'w')

dict_signif = {}
dict_logpval = {}

for key in models:
    # os.system('mkdir ' + output_path + '/' + key)
    if not os.path.exists(output_path  + '/' + key):
        os.mkdir(output_path  + '/' + key)
    dict_signif[key] = {}
    dict_logpval[key] = {}
    path = common_path + key + '/'
    
    if key == 'mexcowalk_orig' or key == 'mexcowalk_beta_02':
       fhmutation = open("../data/genes_vs_patients.txt")
    else:
       fhmutation = open('../data_subtypes/' + c + '/' + c + '.txt')
    
    dict_genes_to_indices = {}
    dict_genes_to_patients = {}
    for line in fhmutation:
       words = line.strip().split()
       gene = words[0]
       indices = words[1:]
       dict_genes_to_indices[gene] = indices
       dict_genes_to_patients[gene] = []
       for index in indices:
	  dict_genes_to_patients[gene].append(dict_indices[index])    


    for N in list_range:
        skip = False
        if key == 'memcover_v3' and N > 1600:
        	skip = True
        if key == 'hier_hotnet' and N not in [554, 806]:
        	skip = True

        if skip == False:
            print ('path is ', path + 'cc_n' + str(N) + '_*')
            comp_filename = glob.glob(path+'cc_n' + str(N) + '_*')[0]
            # print 'comp filename ' ,comp_filename
            fhcomponents = open(comp_filename)
            #fhcomponents = open("../hint/out/connected_components_isolarge_v2/mutex_t07_nsep_cov_nsep/cc_n100_48_10_d0.000327361545892.txt")
            #fhcomponents = open("../../memcover/results/components/cc_n100_memcover_hint_0.548_k0_genes.txt")

            #fhcomponents = open("../../memcover/results/components/cc_n100_memcover_hint_0.03_k0_genes.txt")

            #fhcomponents = open("../hint/out/connected_components_isolarge_v2/hotnet2/cc_n100_k3_d0.000395306627809.txt")



            #fhout = open("../hint/out/cc_n100_memcover_hint_0.548_k0_genes_cancer_subtype_tests.txt", 'w')

            fhout = open(output_path + '/' + key + '/cc_n' + str(N)  + '_cancer_subtype_tests.txt'  , 'w')



            comp_index = 0

            num_components = len(fhcomponents.readlines())
            fhcomponents.seek(0)

            subtype_coverage = [0] * num_subtypes

            sum_logpval = 0
            min_logpvals = []
            pvals = []
            genes_arr =[]
            for component in fhcomponents:
                genes = component.strip().split()
                genes_arr.append(genes)
                #print component
                patient_list_union = []
                for gene in genes:
                	patient_list_union.extend(dict_genes_to_patients[gene])
                #print 'not unique ', len(patient_list_union)
                patient_list_union = list(set(patient_list_union))
                #print 'patient list union unique ', len(patient_list_union)
                count_mutated_subtypes_vec = count_subtype(patient_list_union)
                # fhout.write('COMPONENT is {} {}\n'.format(comp_index, genes))
                # print >>fhout, 'COMPONENT is ', comp_index, genes
                min_pval = 1
                tmp_pvals = []
                for i in range(num_subtypes):
                    entry_11 = count_mutated_subtypes_vec[i]
                    entry_12 = count_types_each[i] - count_mutated_subtypes_vec[i]
                    entry_21 = sum(count_mutated_subtypes_vec) - count_mutated_subtypes_vec[i]
                    entry_22 = count_subtypes_except_this[i] - entry_21
                    oddsratio, pvalue = stats.fisher_exact([[entry_11, entry_12], [entry_21, entry_22]])
                    # print >>fhout, 'subtype ', subtypes[i] , entry_11, entry_12, entry_21, entry_22, pvalue,
                    # corrected_pvalue = pvalue * num_components * num_subtypes
                    tmp_pvals.append(pvalue)
                pvals.append(tmp_pvals)
                comp_index += 1

            pvals= np.array(pvals)

            _,pvals  = fdrcorrection(pvals.reshape(-1,))
            pvals = pvals.reshape(-1,num_subtypes)
            min_pvals = np.log10(pvals.min(axis=1))*-1
            # find the minimix pval
            # take the log(,10)*-1
            # add this to min_plvals[]

            for comp_index,pval_arr in enumerate(pvals):
                fhout.write('COMPONENT is {} {}\n'.format(comp_index, genes_arr[comp_index]))
                for i in range(num_subtypes):
                    logpval = math.log(pval_arr[i], 10)
                    fhout.write('subtype {} {} {}'.format(subtypes[i], pval_arr[i], round(logpval,2)))
                    # print >> fhout, 'subtype', corrected_pvalue, round(logpval,2),
                    if logpval <= min_pval:
                    	min_pval = logpval

                    if pval_arr[i] < 0.05:
                        fhout.write(' ***SIGNIFICANT***\n')
                        # print >>fhout, '***SIGNIFICANT***'
                        subtype_coverage[i] += 1
                    else:
                    	# print >>fhout, ''
                        fhout.write('\n')

                # min_logpvals.append(min_pval)
                # sum_logpval += min_pval


            fhout.write(' '.join([str(xyz) for xyz in  min_pvals]))
            print (subtype_coverage)
            print (sum(subtype_coverage) / float(num_components))
            print (min_pvals.mean())
            dict_signif[key][N] = round(sum(subtype_coverage) / float(num_components),2)
            # dict_logpval[key][N] = round(sum_logpval / float(num_components),2)
            dict_logpval[key][N] = round(min_pvals.mean(),2)
            fhout.close()

fhout_signif_per_comp.write('\t'.join([str(elem) for elem in list_range])+'\n')
fhout_logpval_per_comp.write('\t'.join([str(elem) for elem in list_range])+'\n')
# print >>fhout_signif_per_comp, '\t'.join([str(elem) for elem in list_range])
# print >>fhout_logpval_per_comp, '\t'.join([str(elem) for elem in list_range])
for key in models:
    forprint_signif = key
    forprint_logpval = key
    for N in list_range:
	 forprint_signif += '\t' + str(dict_signif[key][N])
	 forprint_logpval += '\t' + str(dict_logpval[key][N])

    fhout_signif_per_comp.write(forprint_signif+'\n')
    fhout_logpval_per_comp.write(forprint_logpval+'\n')

    # print >>fhout_signif_per_comp, forprint_signif
	# print >>fhout_logpval_per_comp,	forprint_logpval
