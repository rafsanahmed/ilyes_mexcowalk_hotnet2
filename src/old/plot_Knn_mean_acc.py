from utils import *
import glob
import os
import math
import operator

from tqdm import trange, tqdm
from matplotlib import pyplot as plt
#from matplotlib import rcParams
from pylab import *

#ts= [5, 6, 7, 8,9]
#models = ["mutex_t0{}_nsep_cov".format(t) for t in ts]
colors = ['C4', 'C1']#, 'C4', 'C2', 'C3']
axes(frameon=0)
median_idx = 0
mean_idx = 3
#
#models = [
    #"hotnet2",
    #"memcover_v1","memcover_v2","memcover_v3",
    #"mutex_t07_nsep_cov",
    #'hier_hotnet'
     #]
models = ['mexcowalk_hint', 'mexcowalk_intact']
for model in models:
    if model == 'hier_hotnet': continue
    median_scores = []
    mean_scores = []
#
    a = list(range(100,2600,100))
    #a = list(range(100,600,100))+[554]+ list(range(600,900, 100))+[806]
    N=a[:]#N = a + list(range(900, 1700, 100)) if model  == 'memcover_v3' else a+list(range(900, 2600, 100))
    for n in N:
        results_file = '../results/knearst/revision/undersampel/{}/{}.txt'.format(model, n)
        with open(results_file, 'r') as f:
            lines= f.readlines()
            mean = float(lines[mean_idx].rstrip().split()[-1])
            mean_scores.append(mean)
            median = float(lines[median_idx].rstrip().split()[-1])
            median_scores.append(median)
    # print(mean_scores)
    plt.plot(N,mean_scores, '-o'+colors[models.index(model)])
#
median_scores = []
mean_scores = []
#for n in [554,806]:
    #results_file = '../results/knearst/revision/undersampel/{}/{}.txt'.format('hier_hotnet', n)
    #with open(results_file, 'r') as f:
        #lines= f.readlines()
        #mean = float(lines[mean_idx].rstrip().split()[-1])
        #mean_scores.append(mean)
        #median = float(lines[median_idx].rstrip().split()[-1])
        #median_scores.append(median)
## print(mean_scores)
#plt.plot([806],median_scores[1], 'k*', markersize=12)
#plt.plot([554],median_scores[0], 'C8*',markersize=12)
##
##
##
art = []
legend_ = [
        'MEXCOWalk_HINT',
        'MEXCOWalk_IntAct'
        ]
    #"Hotnet2",
    #"MEMCover_v1",
    #"MEMCover_v2",
    #"MEMCover_v3",
    #"MEXCOwalk",
    #"HierHotnet_v1",
    #"HierHotnet_v2"
    #]
#
legend = plt.legend(legend_, loc=8,fancybox=True, fontsize= 'small', framealpha=0,
                    edgecolor = 'b', ncol= 4, bbox_to_anchor=(0.5,-0.3))
art.append(legend)
frame = legend.get_frame()
# frame.set_facecolor('0.9')
# frame.set_edgecolor('0.9')
plt.xlabel('total_genes')
plt.ylabel('Mean Classification Accuracy Score (MCAS)')
xtick = list(range(100,2600,200))#+[554]+ list(range(600,800, 200))+[806]+list(range(900, 2600, 200))
xticks(xtick, fontsize='x-small')
plt.ylim(0.70, 0.782)
# axes(frameon=0)
grid()
plt.savefig('../results/knearst/revision/undersampel/plots/Mean_Accuracy2.pdf', format = 'pdf',additional_artists=art,
            bbox_inches="tight", dpi = 800)
plt.close()
