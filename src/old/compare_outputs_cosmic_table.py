import glob
import os

inpath = '../out/cosmic_similarity/'
infile_sim = inpath + 'cosmic_comparison_hint_vs_inact.txt'
infile_1_hi = inpath + 'PPI_comparison_percentage_hint_vs_intact_percentage.txt'
infile_2_ih = inpath + 'PPI_comparison_percentage_intact_vs_hint_percentage.txt'

models = ['mexcowalk_intact','mexcowalk_hint']
cosmic_file = '../data/Census_allTue_May_23_12-08-15_2017.tsv'

R = range(100,2600,100)

outfile = '../out/cosmic_similarity/' + 'hint_vs_intact_comparison_latex.txt'

d_1 = {}
d_2 = {}

with open(infile_1_hi, 'r') as f:
    for line in f.readlines()[1:]:
        
        line = line.split()
        #print line[0]
        d_1[int(line[0])] = float(line[2])
        
        
with open(infile_2_ih, 'r') as f:
    for line in f.readlines()[1:]:
        
        line = line.split()
        #print line[0]
        d_2[int(line[0])] = float(line[2])
        
#for r in R:
    #print r,d_1[r], d_2[r]
    
with open(infile_sim, 'r') as f, open(outfile, 'w') as of:
    
        lines = f.readlines()
        header = lines[0].split()
        header.insert(1, '&percentage_difference\t')
        print header
        of.write(header[0])
        for hh in header:
            of.write('\t&' + hh)
        of.write('\\\\\n')
        
        for line in lines[1:]:
            line = line.split()
            
            n = int(line[0])
            print n
            
            of.write(line[0] + '\t')
            of.write('&'+'{:.2f}'.format((d_1[n]+ d_2[n])/2.0))
            
            for v in line[1:]:
                of.write('\t&' + v)
            of.write('\\\\\n')
        
    