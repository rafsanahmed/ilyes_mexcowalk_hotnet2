from collections import OrderedDict
import pandas as pd
import matplotlib.pyplot as plt
import glob
import operator
import os


def prep_file_paths(key):
    paths_raw = glob.glob(key)

    dict_paths = {}
    for filename in paths_raw:

        directory, file_core = os.path.split(filename)

        if file_core.startswith('cc'):
            num_genes = int(file_core.split('_')[1][1:])
            dict_paths[num_genes] = filename

    sorted_dict = sorted(dict_paths.items(), key=operator.itemgetter(0))
    print sorted_dict

    paths = []
    for i in range(len(sorted_dict)):
        paths.append(sorted_dict[i][1])
        
    return paths
    
def get_n_from_filepath(filepath):
    
    directory, file_core = os.path.split(filepath)
    
    if file_core.startswith('cc'):
        return int(file_core.split('_')[1][1:])   
    
if __name__ == '__main__':
    
    inpath  = '../out/connected_components_isolarge/'
    plot_path = '../out/figures/'    
    d = OrderedDict()

    models = ['mexcowalk_hint', 'mexcowalk_intact']
    colors = ['C4','C1']
    legend_ = [
        'MEXCOWalk_HINT',
        'MEXCOWalk_IntAct'
        ]
    #models[:]
    
    
    r = range(100,2600,100)
    
    for model in models:
        
        d[model] = OrderedDict()
        
        paths = prep_file_paths(inpath + model + '/*.txt')
        
        for p in paths:
            n = get_n_from_filepath(p)
            if n in r:
                with open(p,'r') as f:
                    
                    num_genes = 0
                    
                    lines = f.readlines()
                    num_comps = float(len(lines))
                    
                    for line in lines:
                        
                        line2 = line.split()
                        num_genes += len(line2)
                    
                    d[model][n] = float(num_genes)/num_comps
        
    for m in d:
        for kk in d[m]:
            print m, kk, d[m][kk]
    
    plt.axes(frameon=0)
    plt.grid()
    plt.xticks(range(100,2600,200))
    #plt.ylim(6, 10.0)
    for m in range(len(models)):
        index = r
        vals = d[models[m]].values()
        
        plt.plot(index, vals, '-o'+colors[m])
    
    art = []
    
    
    legend = plt.legend(legend_, loc=8,fancybox=True, fontsize= 'small', framealpha=0,
                        edgecolor = 'b', ncol= 4, bbox_to_anchor=(0.5,-0.2))
    art.append(legend)
    frame = legend.get_frame()
    
    #plt.show()
    
    plt.savefig('../out/figures/avg_mod_size2.pdf',format = 'pdf', additional_artists=art,
                bbox_inches="tight", dpi = 800)
    plt.close()    